dofile("settings.lua")
local SceneController = inheritsFrom(baseClass)

function SceneController:new(adsController)
  local o = SceneController:create()
  SceneController:init(o,adsController)
  return o
end

function SceneController:init(o,adsController)
  o.list = {}
  --o.adsController = adsController
end

function SceneController:changeScene(name,content)
  if (self.list[name] == nil) then
    self:loadScene(name)
  end
  director:moveToScene(self.list[name])
  dbg.print(content)
  if (content) then
    self.list[name]:apply(content)
  end
  --self.adsController:show()
end

function SceneController:loadScene(name)
  --self.list[name] = dofile("scene_"..name..".lua")
  self.list[name] = require("scene_"..name)
end

--dofile("adsController")
--local adsController = AdsController:new()
sceneController = SceneController:new(adsController)
sceneController:changeScene("MainMenu")