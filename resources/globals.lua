dbg.print("globals.lua")

--SCALES
PROCENTAGE_SCALE = 100
MAIN_SCALE = PROCENTAGE_SCALE*director.displayHeight/516/100

--MAP
EDITOR_MAP_NAME = "mapa"
EDITOR_MAP_WIDTH = 32 --1024px
EDITOR_MAP_HEIGHT = 25 --800px
EDITOR_PLAYER_NUMBER = 4
ATLAS_LENGTH = 2

inspect = dofile("inspect.lua")

function powerOfTen(power)
  local result = 1
  for i = 1,power do
    result = result * 10
  end
  return result
end

function distanceBetweenPoints(p1,p2)
  return math.sqrt((p1.x-p2.x)^2+(p1.y-p2.y)^2)
end

--PHYSICS
PHYSICS_CONST = powerOfTen(2)
X_VELOCITY_CONST = 3.5*PHYSICS_CONST
Y_VELOCITY_CONST = 5*PHYSICS_CONST
Y_ACCELERATION_CONST = 10*PHYSICS_CONST

--CHARACTER
CHAR_WIDTH = 28
CHAR_HEIGHT = 48
MAX_HP = 100
CHARACTER_NAME = "user"..math.random(100)
RESPAWN_TIME = 3 --seconds

--PHOTON_CLIENT
PHOTON_CLIENT_ROTATION_MULTIPLIER = 5
PHOTON_CLIENT_SERVICE_TIMEOUT = 1 --seconds

--WEAPON
COOLDOWN = 1.5--seconds
MAX_DAMAGE = 50
DAMAGE_DISTANCE = 96

--UI-BARS
UI_BARS_WIDTH = 10
UI_BARS_MARGIN = 3

--Stack class
List = inheritsFrom(baseClass)

function List:new()
  local o = List:create()
  List:init(o)
  return o
end

function List:init(o)
  o.table = {}
  --dbg.print("List length "..#o.table)
end

function List:push(item)
  self.table[#self.table+1] = item
  --dbg.print("List length "..#self.table)
end

function List:pop()
  --dbg.print("List length "..#self.table)
  return table.remove(self.table,1)
end

function List:isEmpty()
  --dbg.print("List length "..#self.table)
  return (#self.table == 0)
end

