dbg.print("adsController.lua")

AdsController = inheritsFrom(baseClass)

function AdsController:new()
  local o = AdsController:create()
  AdsController:init(o)
  return o
end

function AdsController:init(o)
  o.AD_ID = 180578822
  o.AD_PROVIDER = "leadbolt"
  o.AD_TYPE = "wall"
end

function AdsController:show()
  if ads:isAvailable() then
    --ads:newAd(0, director.displayHeight, self.AD_PROVIDER, self.AD_TYPE, self.AD_ID)
    --ads:show(false)
  else
    dbg.print("Ads are not available")
  end
end