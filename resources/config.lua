config =
{
    debug =
    {
        general = true,
        makePrecompiledLua = true,  -- turn ON precompilation of lua files
        usePrecompiledLua = true,   -- turn ON use of precompiled lua files
        useConcatenatedLua = true,
    }
}