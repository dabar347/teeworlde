dbg.print("physicsInterface.lua")

PhysicsInterface = inheritsFrom(baseClass)

function PhysicsInterface:new(map,player)
  local o = PhysicsInterface:create()
  PhysicsInterface:init(o,map,player)
  return o
end

function PhysicsInterface:init(o,map,player)
  o.map = map
  o.player = player
  physics:setGravity(0, -Y_ACCELERATION_CONST)
  physics:setAllowSleeping(false)
  self:attachPlayer(player)
  self:attachMap(map)
end

function PhysicsInterface:attachPlayer(player)
  physics:addNode(player.sprite,{isSensor = false, friction = 0, restitution = 0})
  player.sprite:addEventListener("collision",weaponController_collisionHandler)
end

function PhysicsInterface:attachMap(map)
  --[[for y = 1,map.mapObject.mapHeight do
    for x = 1,map.mapObject.mapWidth do
      if (map.sprite.spriteMatrix[y][x]) then physics:addNode(map.sprite.spriteMatrix[y][x],{type = 'static', isSensor = true, friction = 0, restitution = 0}) end
    end
  end]]
  physics:addNode(map.sprite,{type = 'static', isSensor = true, friction = 0, restitution = 0}) 
  for y = 1,map.mapObject.mapHeight do
    for x = 1,map.mapObject.mapWidth do
      if (map.sprite.spriteMatrix[y][x]) then 
        map.sprite.spriteMatrix[y][x]:addEventListener("collision",weaponController_collisionHandler) 
        map.sprite.spriteMatrix[y][x]._debug_ = "MAP"
      end
    end
  end
  --map.sprite:addEventListener("collision",weaponController_collisionHandler)
end

function PhysicsInterface:update()
  --dbg.print(self.player.toBeRespawned)
  if (self.player.toBeRespawn) then
    local spawnPoints = self.map:getSpawnPoints()
    math.randomseed(os.time())
    local n = math.random(#spawnPoints+1) - 1
    self.player.sprite.physics:setTransform((spawnPoints[n].x-1)*32+player.sprite.w/2,(spawnPoints[n].y-1)*32+player.sprite.h/2+16,0)
    self.player.toBeRespawn = false
  else
    local vy = 0
    if self.player.vy == 0 then
      dt, vy = self.player.sprite.physics:getLinearVelocity()
    else
      vy = self.player.vy
      self.player.vy = 0
    end
    self.player.sprite.physics:setTransform(self.player.sprite.x,self.player.sprite.y,0)
    --dbg.print(vy)
    self.player.sprite.physics:setLinearVelocity(self.player.vx,vy)
  end
  --self.player.vy = self.player.vy - Y_ACCELERATION_CONST
  --[[local data = {
    player = {
      nick = player.nick,
      x = player.x,
      y = player.y
    },
    shoot = nil,
    type = "current"
  }
  --dbg.print(type(photonClient))
  photonClient:update(data)
  photonClient:service()]]
end