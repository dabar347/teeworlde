--require("mobdebug").start()

print("settings.lua")

dofile("globals.lua")
dofile("map.lua")
dofile("player.lua")
dofile("camera.lua")
dofile("physicsInterface.lua")
dofile("control.lua")
dofile("weaponController.lua")
dofile("UI_bars.lua")
