local LoadBalancingClient
local LoadBalancingConstants
local tableutil
local Logger
local photon

dbg.print("Demo: main module:","Lua lib used")
photon = require("photon")
LoadBalancingClient = require("photon.loadbalancing.LoadBalancingClient")
LoadBalancingConstants = require("photon.loadbalancing.constants")
Actor = require("photon.loadbalancing.Actor")
Logger = require("photon.common.Logger")
tableutil = require("photon.common.util.tableutil")

local appInfo = require("cloud-app-info")

local EVENT_CODE = 101
local MAX_SENDCOUNT = 100

local client = LoadBalancingClient.new(appInfo.MasterAddress, appInfo.AppId, appInfo.AppVersion)
--client._myActor.name = CHARACTER_NAME
client:setLogLevel(0)
local lastErrMess = ""

client.queue = List:new()
--[[function client:onOperationResponse(errorCode, errorMsg, code, content)
    self.logger:debug("onOperationResponse", errorCode, errorMsg, code, tableutil.toStringReq(content))
    if errorCode ~= 0 then
        if code == LoadBalancingConstants.OperationCode.JoinRandomGame then  -- master random room fail - try create
            self.logger:info("createRoom")
            self:createRoom("helloworld")
        end
        if code == LoadBalancingConstants.OperationCode.CreateGame then -- master create room fail - try join random
            self.logger:info("joinRandomRoom - 2")
            self:joinRandomRoom()
        end
        if code == LoadBalancingConstants.OperationCode.JoinGame then -- game join room fail (probably removed while reconnected from master to game) - reconnect
            self.logger:info("reconnect")
            self:disconnect()
        end
    end
end]]

function client:attachPlayer(player)
  self.player = player
end

function client:attachWeaponController(wc)
  self.weaponController = wc
end

function client:update()
  if (self.player) then
    local updateData = {
                          player = self.player.nick,
                          type = "update",
                          x = math.floor(self.player.sprite.x),
                          y = math.floor(self.player.sprite.y),
                          rotation = math.floor(self.player.sprite.rotation/PHOTON_CLIENT_ROTATION_MULTIPLIER),
                          multiplier = PHOTON_CLIENT_ROTATION_MULTIPLIER
                        }
    --dbg.print(inspect(updateData))
    --dbg.print(not(inspect(self.player.lastUpdate) == inspect(updateData)))
    if not(inspect(self.player.lastUpdate) == inspect(updateData)) then
      --dbg.print("Send update")
      self.player.lastUpdate = updateData
      self:raiseEvent(EVENT_CODE, updateData, { receivers = LoadBalancingConstants.ReceiverGroup.All } ) 
      self.lastTime = system:getTime()
    end
  end
  if (self.weaponController) then
    for _, b in pairs(self.weaponController.storage) do
      local updateData = {
                            player = self.player.nick,
                            type = "update",
                            bulletId = b.id,
                            x = math.floor(b.x),
                            y = math.floor(b.y),
                            rotation = math.floor(b.rotation/PHOTON_CLIENT_ROTATION_MULTIPLIER),
                            multiplier = PHOTON_CLIENT_ROTATION_MULTIPLIER
                          }
       --dbg.print("Bullet update sent"..inspect(_).." "..inspect(b))
       self:raiseEvent(EVENT_CODE, updateData, { receivers = LoadBalancingConstants.ReceiverGroup.All } ) 
       self.lastTime = system:getTime()
    end
  end
  if not(self.queue:isEmpty()) then
    dbg.print("Queue event")
    self:raiseEvent(EVENT_CODE, self.queue:pop(), { receivers = LoadBalancingConstants.ReceiverGroup.All } ) 
    self.lastTime = system:getTime()
  end
  if (system:getTime() - self.lastTime) >= PHOTON_CLIENT_SERVICE_TIMEOUT then
    self:raiseEvent(EVENT_CODE, {type="service"}, { receivers = LoadBalancingConstants.ReceiverGroup.All } ) 
    self.lastTime = system:getTime()
  end
end

function client:onActorJoin(actor)
  self.player.lastUpdate = nil
end

function client:onActorLeave(actor)
  --dbg.print(inspect(actor.loadBalancingClient.player.nick))
  if (enemies[actor.loadBalancingClient.player.nick]) then
    physics:removeNode(enemies[actor.loadBalancingClient.player.nick])
    enenemies[actor.loadBalancingClient.player.nick] = enemies[actor.loadBalancingClient.player.nick]:removeFromParent()
  end
end

function client:onJoinRoom()
  system:sendEvent("joinRoom")
end

function client:onStateChange(state)
    --dbg.print("Demo: onStateChange " .. state .. ": " .. tostring(LoadBalancingClient.StateToName(state)))
    if state == LoadBalancingClient.State.JoinedLobby then
        system:sendEvent("connectionSucced")
    end
    --The place where this jerk joins the lobby
end

function client:onRoomList(rooms)
  system:sendEvent("roomList")
end

function client:onError(errorCode, errorMsg)
    if errorCode == LoadBalancingClient.PeerErrorCode.MasterAuthenticationFailed then
        errorMsg = errorMsg .. " with appId = " .. self.appId .. "\nCheck app settings in cloud-app-info.lua"
    end
    self.logger:error(errorCode, errorMsg)
    lastErrMess = errorMsg;
end

function client:onEvent(code, _content, actorNr)
    --self.logger:debug("on event", code, tableutil.toStringReq(content))
    if code == EVENT_CODE then
        --[[client.mReceiveCount = client.mReceiveCount + 1
        client.mLastReceiveEvent = content[2]
        if client.mReceiveCount == MAX_SENDCOUNT then
            self.mState = "Data Received"    
            client:disconnect()
        end]]
         --dbg.print(inspect(_content.type).." "..inspect(_content.bulletId))
         system:sendEvent("photonClientEvent",{content = _content})
    end
end

client.mState = "Init"
client.mLastSentEvent = ""
client.mSendCount = 0
client.mReceiveCount = 0
client.mLastReceiveEvent= ""
client.mRunning = true
client.lastTime = 0

function client:getStateString()
    return LoadBalancingClient.StateToName(self.state) .. "\n\nevents: " .. self.mState .."\nsent "..self.mLastSentEvent..", total: ".. self.mSendCount .. "\nreceived "..self.mLastReceiveEvent .. ", total: " .. self.mReceiveCount 
        .. "\n\n" .. lastErrMess
end
local prevStr = ""
function client:timer(event)
    self:service()
    self:update()
end

client:connect()

function photonClient_timerListener(event)
  client:timer()
end
system:addTimer(photonClient_timerListener,0.02)

return client

--[[

PACKAGE
{
  player = UN,
  type = "update/emit/explode/damage/frag",
  x = _x,
  y = _y,
  <bulletId = wUN>
  <reciever = rUN>,
  <deltDamage = n>
}
]]

