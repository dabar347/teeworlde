-- Your app starts here!
dbg.print("camera.lua")

Camera = inheritsFrom(baseClass)

function Camera:new(rootNode,player)
  local o = Camera:create()
  Camera:init(o,rootNode,player)
  return o
end

function Camera:init(o,rootNode,player)
  o.rootNode = rootNode
  o.player = player
end

function Camera:update()|
  
	local centerX = director.displayCenterX
	local centerY = director.displayCenterY
	self.rootNode.x = centerX - self.player.sprite.x*MAIN_SCALE
	self.rootNode.y = centerY - self.player.sprite.y*MAIN_SCALE
end

