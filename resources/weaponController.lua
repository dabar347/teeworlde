WeaponController = inheritsFrom(baseClass)

function WeaponController:new(player,map)
  local o = WeaponController:create()
  WeaponController:init(o,player,map)
  return o
end

function WeaponController:init(o,player,map)
  o.player = player
  o.map = map
  o.node = director:createNode()
  o.isWC = true
  o.nodeToRemove = List:new()
  o.storage = {}
  o.id = 0
  o.lastTime = 0
  --dbg.print(inspect(o.player))
end

function WeaponController:update()
  local bullet
  while (not(self.nodeToRemove:isEmpty())) do
    bullet = self.nodeToRemove:pop()
    physics:removeNode(bullet)
    self.storage[bullet.id] = nil
    bullet = bullet:removeFromParent()
  end
  collectgarbage()
end

function weaponController_collisionHandler(event)
  --dbg.print(inspect(event.nodeA.isBullet).." "..inspect(event.nodeB.isBullet))
  local bullet
  --dbg.print(inspect(event.nodeA._debug_).." "..inspect(event.nodeB._debug_))
  if (event.nodeA.isBullet) then bullet = event.nodeA
  elseif (event.nodeB.isBullet) then bullet = event.nodeB end
  if (event.phase == "began" and bullet) then
    if(bullet.alive) then 
      --Destroy
      bullet.wc.nodeToRemove:push(bullet)
      bullet.alive = false
      local destroyData = {
                            player = bullet.wc.player.nick,
                            x = bullet.x,
                            y = bullet.y,
                            type = "explode",
                            bulletId = bullet.id,
                          }
      photonClient.queue:push(destroyData)
      --Animate
    
      --Deal damage
      ----Me
      local dmg = MAX_DAMAGE - MAX_DAMAGE*distanceBetweenPoints(bullet.wc.player.sprite,bullet)/DAMAGE_DISTANCE
      if (dmg >= 0) then
        bullet.wc.player:dealDamage(dmg,bullet.wc.player.nick)
      end
      ----Others
      --[[if (enemies) then
        --dbg.print(inspect(enemies))
        for _, p in pairs(enemies) do
          local dmg = MAX_DAMAGE - MAX_DAMAGE*distanceBetweenPoints(p.sprite,bullet)/DAMAGE_DISTANCE
          dbg.print(p.nick,dmg)
          if (dmg >= 0) then
            --bullet.wc.player:dealDamage(dmg,bullet.wc.player.nick)
            local damageData = {
                                player = bullet.wc.player.nick,
                                type = "damage",
                                reciever = p.nick,
                                deltDamage = dmg
                                }
            photonClient.queue:push(damageData)
          end
        end
      end]]
    end
  end
end

function weaponController_createBullet(_id,_x,_y,angle)
  local bullet = director:createSprite({
      x = 0,
      y = 0,
      xAnchor = 0.5,
      yAnchor = 0,
      source = "f_player.png",
      id = _id
    })
  return bullet
end

function WeaponController:emit(_x,_y,angle)
  if (system:getTime() - self.lastTime) > COOLDOWN then
    local bullet = director:createSprite({
        x = _x,
        y = _y,
        xAnchor = 0.5,
        yAnchor = 0,
        source = "player.png",
        isBullet = true,
        alive = true,
        wc = self,
        id = self.id,
        rotation = angle
      })
    bullet._debug_ = "BULLET"
    --dbg.print(bullet.x.." "..bullet.y)
    self.node:addChild(bullet)
    physics:addNode(bullet,{isSensor = false})
    if (angle < 0) then angle = angle + 360 end
    --local playerRadius = math.sqrt(self.player.sprite.w*self.player.sprite.w + self.player.sprite.h*self.player.sprite.h)/2 + 3
    local playerRadius = math.sqrt(self.player.sprite.w*self.player.sprite.w + self.player.sprite.h*self.player.sprite.h)/2 + 3
    radAngle = math.rad(angle)
    --dbg.print(self.player.sprite.x.." "..bullet.x.." "..math.cos(radAngle)*playerRadius)
    bullet.physics:setTransform(bullet.x+math.cos(radAngle)*playerRadius,bullet.y+math.sin(radAngle)*playerRadius,90-angle)
    
    local v = Y_ACCELERATION_CONST/1.5
    local vx = math.cos(radAngle)*v
    local vy = math.sin(radAngle)*v
    bullet.physics:setLinearVelocity(vx,vy)
    
    --bullet.physics.isSensor = true
    
    bullet:addEventListener("collision",weaponController_collisionHandler)
    self.storage[self.id] = bullet
    self.id = self.id + 1 
    --self.player.sprite:addEventListener("collision",weaponController_collisionHandler)
    --self.map.sprite:addEventListener("collision",weaponController_collisionHandler)
    self.lastTime = system:getTime()
  end
end