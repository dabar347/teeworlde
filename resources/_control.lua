local controlNode = director:createNode({zOrder = 100})


leftControl = director:createSprite({
    x = 2*px,
    y = 5*px,
    zOrder = 5,
    xScale = px/2,
    yScale = px/2,
    source="sprites/buttons/left.png"
  })

rightControl = director:createSprite({
    x = director.displayWidth-15*px,
    y = 5*px,
    zOrder = 5,
    xScale = px/2,
    yScale = px/2,
    source="sprites/buttons/right.png"
  })

actionControl = director:createSprite({
    x = director.displayWidth-26*px,
    y = 1,
    zOrder = 5,
    xScale = px/2,
    yScale = px/2,
    source="sprites/buttons/use.png"
  })

inventoryControl = director:createSprite({
    x = 15*px,
    y = 1,
    zOrder = 5,
    xScale = px/2,
    yScale = px/2,
    source="sprites/buttons/inv.png"
  })

--[[function enableInventory(event)
  if event.phase == "ended" then director:moveToScene(inventoryScene,{transitionType="fade", transitionTime=0.2}) end
end
inventoryControl:addEventListener("touch",enableInventory)]]--

controlNode:addChild(leftControl)
controlNode:addChild(rightControl)
controlNode:addChild(actionControl)
controlNode:addChild(inventoryControl)

leftControl:getAtlas():setTextureParams("GL_NEAREST", "GL_NEAREST")
rightControl:getAtlas():setTextureParams("GL_NEAREST", "GL_NEAREST")
actionControl:getAtlas():setTextureParams("GL_NEAREST", "GL_NEAREST")
inventoryControl:getAtlas():setTextureParams("GL_NEAREST", "GL_NEAREST")


--Shade buttons
shadeCircle = director:createCircle({
    x = 15*px,
    y = 1,
    radius = 10,
    alpha = 0.5,
    color = color.black,
    xScale = px/2,
    yScale = px/2,
    strokeWidth = 0,
    isVisible = false,
    zOrder = 100,
   })
 controlNode:addChild(shadeCircle)
 
 function inventoryControlShade(event)
   if event.phase == "began" then
     shadeCircle.x = 15*px
     shadeCircle.y = 1
     shadeCircle.radius = 10
     shadeCircle.isVisible = true
   elseif event.phase == "ended" then
     shadeCircle.isVisible = false
   end
 end
 function actionControlShade(event)
   if event.phase == "began" then
     shadeCircle.x = director.displayWidth-26*px
     shadeCircle.y = 1
     shadeCircle.radius = 10
     shadeCircle.isVisible = true
   elseif event.phase == "ended" then
     shadeCircle.isVisible = false
   end
 end
  function leftControlShade(event)
   if event.phase == "began" then
     shadeCircle.x = 2*px
     shadeCircle.y = 5*px
     shadeCircle.radius = 12
     shadeCircle.isVisible = true
   elseif event.phase == "ended" then
     shadeCircle.isVisible = false
   end
 end
 function rightControlShade(event)
   if event.phase == "began" then
     shadeCircle.x = director.displayWidth-15*px
     shadeCircle.y = 5*px
     shadeCircle.radius = 12
     shadeCircle.isVisible = true
   elseif event.phase == "ended" then
     shadeCircle.isVisible = false
   end
 end
 inventoryControl:addEventListener("touch",inventoryControlShade)
 actionControl:addEventListener("touch",actionControlShade)
 leftControl:addEventListener("touch",leftControlShade)
 rightControl:addEventListener("touch",rightControlShade)

return controlNode