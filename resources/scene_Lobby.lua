local scene = director:createScene()
director.addNodesToScene = true
local sceneRoot = director:createNode({xScale = MAIN_SCALE, yScale = MAIN_SCALE})
director.addNodesToScene = false
--SCENE HEADER
--Requires
--dofile("globals")
----WORKFLOW
--Establish connection
photonClient = dofile("photonClient.lua")
local lobby_Button = inheritsFrom(baseClass)

function lobby_Button:new(table)
  --dbg.print(inspect(table))
  local o = lobby_Button:create()
  lobby_Button:init(o,table)
  return o
end

function lobby_Button:init(o,table)
  o.name = table.name
  o.sprite = director:createNode({xScale = MAIN_SCALE, yScale = MAIN_SCALE})
  o.sprite.back = director:createRectangle({
      w = 715/2,
      h = 516/4,
      strokeWidth = 0,
      scene = table.scene,
      content = table.content
    })
  o.sprite.back.text = director:createLabel({
      xAnchor = 0.5,
      yAnchor = 0.5,
      text = table.name,
      x = o.sprite.back.w/2,
      y = o.sprite.back.h/2,
      color = color.red,
      hAlignment="center", vAlignment="center"
    })
  o.sprite.back.shade = director:createRectangle({
      w = 715/2,
      h = 516/4,
      strokeWidth = 0,
      alpha = 0.5,
      color = color.black,
      isVisible = false
    })
  o.sprite:addChild(o.sprite.back)
  o.sprite.back:addChild(o.sprite.back.text)
  o.sprite.back:addChild(o.sprite.back.shade)
end

local buttonsArray = {{name = "CREATE ROOM", func = scene_lobby_createRoom}, {name = "JOIN ROOM", func = scene_lobby_joinRoom}}
for i = 1,#buttonsArray do
  buttonsArray[i] = lobby_Button:new(buttonsArray[i])
  buttonsArray[i].sprite.x = director.displayCenterX - (715/4 * MAIN_SCALE)
  buttonsArray[i].sprite.y = (516 - (516/16 + 516/4 + (i-1)*(516/4+516/16))) * MAIN_SCALE
  sceneRoot:addChild(buttonsArray[i].sprite)
end
--Handlers
function scene_Lobby_buttonHandler(event)
  if (event.phase == "began") then 
    event.target.shade.isVisible = true
  else
    event.target.shade.isVisible = false
  end
end
for i = 1,#buttonsArray do
  buttonsArray[i].sprite.back:addEventListener("touch",scene_Lobby_buttonHandler)
end

function scene_lobby_createRoom()
  photonClient:createRoom("TEST",{customGameProperties = {map = "mapa"}})
end

function scene_lobby_joinRoom()
  photonClient:joinRoom("TEST")
end

buttonsArray[1].sprite.back:addEventListener("touch",scene_lobby_createRoom)
buttonsArray[2].sprite.back:addEventListener("touch",scene_lobby_joinRoom)

function scene_Lobby_onPhotonClientConnectionSucced(event)
  print("Connection succed; Refreshing list")
  sceneRoot:addChild(director:createCircle(10,10,10))
end

function scene_Lobby_onPhotonClientRoomList(event)
  dbg.print("Room list has recieved")
  --dbg.print(inspect(photonClient:availableRooms()))
end

function scene_Lobby_onPhotonJoinedRoom(event)
  dbg.print(inspect(photonClient:myRoom().customProperties.map))
  sceneController:changeScene("Game",photonClient:myRoom().customProperties.map)
end
system:addEventListener("connectionSucced",scene_Lobby_onPhotonClientConnectionSucced)
system:addEventListener("roomList",scene_Lobby_onPhotonClientRoomList)
system:addEventListener("joinRoom",scene_Lobby_onPhotonJoinedRoom)
--TEST ONLY
--[[function _TEST_keyListener(event)
  --dbg.print(event.keyCode)
  if (event.phase == "pressed") then
  if (event.keyCode == key.P) then
    dbg.print("Create room")
    photonClient:createRoom("TEST",{customGameProperties = {map = "mapa"}})
  elseif (event.keyCode == key.L) then
    photonClient:joinRoom("TEST")
  end
  end
end
system:addEventListener("key",_TEST_keyListener)]]
--SCENE FOOTER
return scene