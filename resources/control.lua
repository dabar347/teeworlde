dbg.print("control.lua")

--require("control_touch")

Control = inheritsFrom(baseClass)

function control_touchListener(event)
  if (event.phase == "began") then
    --dbg.print(inspect(event))
    event.target.center.isVisible = true
    event.target.center.x = event.x - event.target.x + event.target.radius
    event.target.center.y = event.y - event.target.y + event.target.radius
    if (event.target.type == "movement") then
      if (event.target.center.x - event.target.radius) > 0 then
        player:setMotion("right")
      elseif (event.target.center.x - event.target.radius) < 0 then
        player:setMotion("left")
      end
      if (event.target.center.y - event.target.radius) > (event.target.radius / 2) then
        player:setMotion("jump")
      end
    end
  elseif (event.phase == "ended") then
    event.target.center.isVisible = false
    if (event.target.type == "movement") then
      player:setMotion("still")
    elseif (event.target.type == "weapon") then
      --dbg.print((event.target.center.y - event.target.radius).." "..(event.target.center.x - event.target.radius))
      --dbg.print(math.deg(math.atan2((event.target.center.y - event.target.radius),(event.target.center.x - event.target.radius))))
      event.target.control.weaponController:emit(event.target.control.player.sprite.x,event.target.control.player.sprite.y,math.deg(math.atan2((event.target.center.y - event.target.radius),(event.target.center.x - event.target.radius))))
    end
  elseif (event.phase == "moved") then
    event.target.center.isVisible = true
    event.target.center.x = event.x - event.target.x + event.target.radius
    event.target.center.y = event.y - event.target.y + event.target.radius
    if (event.target.type == "movement") then
      if (event.target.center.x - event.target.radius) > 0 then
        player:setMotion("right")
      elseif (event.target.center.x - event.target.radius) < 0 then
        player:setMotion("left")
      end
      if (event.target.center.y - event.target.radius) > (event.target.radius / 2) then
        player:setMotion("jump")
      end
    end
  end
end

function Control:new(player,weaponController)
  local o = Control:create()
  Control:init(o,player,weaponController)
  return o
end

function Control:init(o,player,weaponController)
  o.player = player
  o.weaponController = weaponController
  o.sprite = director:createNode()
  o:drawTouch()
  o.sprite.leftStick:addEventListener("touch",control_touchListener)
  o.sprite.rightStick:addEventListener("touch",control_touchListener)
end

function Control:drawCircle()
  local sprite
  sprite = director:createCircle(0,0,director.displayHeight*0.2)
  sprite.xAnchor = 0.5
  sprite.yAnchor = 0.5
  sprite.alpha = 0.5
  local center = director:createCircle(0,0,director.displayHeight*0.2*0.3)
  center.xAnchor = 0.5
  center.yAnchor = 0.5
  center.alpha = 0.5
  center.isVisible = false
  sprite.center = center
  sprite:addChild(center)
  return sprite
end


function Control:drawTouch()
  self.sprite.leftStick = self:drawCircle()
  self.sprite.leftStick.x = director.displayHeight*0.2+3
  self.sprite.leftStick.y = director.displayHeight*0.2+3
  self.sprite:addChild(self.sprite.leftStick)
  self.sprite.leftStick.control = self
  self.sprite.leftStick.type = "movement"
  
  self.sprite.rightStick = self:drawCircle()
  self.sprite.rightStick.x = director.displayWidth-(director.displayHeight*0.2+3)
  self.sprite.rightStick.y = director.displayHeight*0.2+3
  self.sprite:addChild(self.sprite.rightStick)
  self.sprite.rightStick.control = self
  self.sprite.rightStick.type = "weapon"
end

function Control:keyListener(event)
  --dbg.print(event.keyCode)
  if (event.keyCode == key.W) then
    if (event.phase == "pressed") then
      self.player:setMotion("jump")
    end
  elseif (event.keyCode == key.A) then
    if (event.phase == "pressed") then
      self.player:setMotion("left")
    else
      self.player:setMotion("still")
    end
  elseif (event.keyCode == key.D) then
    if (event.phase == "pressed") then
      self.player:setMotion("right")
    else
      self.player:setMotion("still")
    end
  elseif(event.keyCode == key.O) then
    if (event.phase == "pressed") then
      self.weaponController:emit(0)
    end
  elseif(event.keyCode == key.H) then
    if (event.phase == "pressed") then
      dbg.print("HP: "..self.player.hp)
    end
  elseif(event.keyCode == key.F) then
    if (event.phase == "pressed") then
      dbg.print("Frags: "..self.player.frags)
    end
  end
end