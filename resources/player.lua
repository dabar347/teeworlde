-- Your app starts here!
dbg.print("player.lua")

Player = inheritsFrom(baseClass)

function Player:new(config)
  local o = Player:create()
  Player:init(o,config)
  return o
end

function Player:init(o,config)
  o:createCharacter(config)
  o.vy = 0
  o.vx = 0
end


function Player:createCharacter(config)
	local player = director:createSprite({
	    w = CHAR_WIDTH, --CHAR_HEIGHT/2
	    h = CHAR_HEIGHT,
	    --zOrder = 2,
      isPlayer = true,
	    xAnchor = 0.5,
	    yAnchor = 0.5,
	    source=config.source..".png",
	    grounded = 0,
      time = 0,
      _debug_ = "PLAYER"
	  })
  --dbg.print(player.w)
  math.randomseed(os.time())
  self.sprite = player
  self.nick = "user"..math.random(100)
  self.hp = MAX_HP
  self.frags = 0
  self.toBeRespawn = false
end

function Player:setMotion(motion)
  if motion == "left" then
    self.vx = -X_VELOCITY_CONST
  elseif motion == "right" then
    self.vx = X_VELOCITY_CONST
  elseif motion == "still" then
    self.vx = 0
  elseif motion == "jump" then
    local vx
    local vy
    vx, vy = self.sprite.physics:getLinearVelocity()
    if (math.floor(math.abs(vy)) == 0) then self.vy = Y_VELOCITY_CONST end
  end
end

function Player:dealDamage(dmg,source)
  self.hp = self.hp - dmg
  if (self.hp <= 0) then
    if (source == self.nick) then
      self.frags = self.frags - 1
    else
      --emit frag event
    end
    self.hp = MAX_HP
    respawn(player,map)
  end
end

--[[function Player:collisionHandler(event)
  --dbg.print(inspect(event))
  if (event.phase == "began") then
    --bottom of player - player.sprite.y - player.sprite.h*player.sprite.yAnchor
    --top of block - event.nodeB.y + event.nodeB.h
    if ((self.sprite.y - self.sprite.h*self.sprite.yAnchor) >= (event.nodeB.y + event.nodeB.h)) and (self.vy <= 0) then 
      self.grounded = self.grounded + 1
    end
  else
    if ((self.sprite.y - self.sprite.h*self.sprite.yAnchor) >= (event.nodeB.y + event.nodeB.h)) and (self.vy <= 0) then 
      self.grounded = self.grounded - 1
    end
  end
  --dbg.print(self.grounded)
end]]

--OLD ONES DOWN THERE--
