UIBar = inheritsFrom(baseClass)

function UIBar:new(color)
  local o = UIBar:create()
  UIBar:init(o,color)
  return o
end

function UIBar:init(o,color)
  o.color = color
  o.oLength = director.displayWidth*0.6*MAIN_SCALE
  o.node = director:createNode()
  o.node.rectangle = director:createRectangle({
      yAnchor = 1,
      h = UI_BARS_WIDTH,
      w = o.oLength,
      x = 0,
      y = 0,
      strokeWidth = 0,
      color = o.color})
  o.node:addChild(o.node.rectangle)
end

function UIBar:set(value)
  self.node.rectangle.w = self.oLength * value / 100
end

UIBarContainer = inheritsFrom(baseClass)

function UIBarContainer:new(player,wc)
  local o = UIBarContainer:create()
  UIBarContainer:init(o,player,wc)
  return o
end

function UIBarContainer:init(o,player,wc)
  o.player = player
  o.wc = wc
  o.node = director:createNode({xScale = MAIN_SCALE, yScale = MAIN_SCALE})
  o.bars = {}
  o.bars.hp = UIBar:new(color.red)
  o.bars.cooldown = UIBar:new(color.green)
  o.node:addChild(o.bars.hp.node)
  o.bars.hp.node.x = 3
  o.bars.hp.node.y = director.displayHeight - (UI_BARS_MARGIN)
  o.node:addChild(o.bars.cooldown.node)
  o.bars.cooldown.node.x = 3
  o.bars.cooldown.node.y = director.displayHeight - (UI_BARS_WIDTH + UI_BARS_MARGIN*2)
end

function UIBarContainer:update()
  self.bars.hp:set(self.player.hp)
  if (system:getTime() - self.wc.lastTime) > COOLDOWN then
    self.bars.cooldown:set(100)
  else
    self.bars.cooldown:set((system:getTime() - self.wc.lastTime)/COOLDOWN*100)
  end
end