-- Your app starts here!
--require("_config")
--require("autochat-lb")
photonClient = dofile("photonClient.lua")

--dbg.print(require("inspect")(photonClient))



if (not(dofile("editor.lua"))) then --EDITOR_MODE check

physics:setGravity(0, 0)
physics:setAllowSleeping(false)

px = director.displayHeight/64

root = director:createNode({xScale = MAIN_SCALE, yScale = MAIN_SCALE})
ui = director:createNode()
director.addNodesToScene = false

--physics:setScale(1)

require("player")
require("map")
require("camera")

local control = require("control")
ui:addChild(control)

--map loading
map = loadMap("mapa")
root:addChild(map)
physics:addNode(map,{type = 'static', isSensor = false, friction = 0, restitution = 0})

--player init
player = createCharacter({source="player"})
setPlayerCoord(player,350,500)
root:addChild(player)
physics:addNode(player,{isSensor = false, friction = 0, restitution = 0, density = 10})
dbg.print(require("inspect")(player.physics))
--controls
----Keyboard
function keyListener(event)
  --dbg.print(event.keyCode)
  if (event.keyCode == key.W) then
    if (event.phase == "pressed") then
      jump(player)
    end
  elseif (event.keyCode == key.A) then
    if (event.phase == "pressed") then
      leftHorizontal(player)
    else
      stopHorizontal(player)
    end
  elseif (event.keyCode == key.D) then
    if (event.phase == "pressed") then
      rightHorizontal(player)
    else
      stopHorizontal(player)
    end
  end
end
system:addEventListener("key",keyListener)

--test
leftControl:addEventListener("touch",leftEventListener)
rightControl:addEventListener("touch",rightEventListener)
function testJump(event)
  if (event.phase == "began") then jump(player) end
end
actionControl:addEventListener("touch",testJump)
system:addEventListener("update",updatePlayer)
map:addEventListener("collision",playerSensor)

end --EDITOR_MODE check
