local scene = director:createScene()
director.addNodesToScene = true
--local sceneRoot = director:createNode({xScale = MAIN_SCALE, yScale = MAIN_SCALE})
local sceneRoot = director:createNode()
local controlRoot = director:createNode()
director.addNodesToScene = false
--SCENE HEADER
--Requires
--dofile("globals")

scene_game_nodesToRemove = List:new()


function respawn(event)
  if (event.timer) then
    --Draw countdown!
    print(inspect(RESPAWN_TIME + 1 - event.doneIterations))
    if (RESPAWN_TIME + 1 - event.doneIterations == 0) then
      local spawnPoints = map:getSpawnPoints()
      math.randomseed(os.time())
      local n = math.random(#spawnPoints+1) - 1
      if (player.sprite.physics == nil) then
        player.sprite.x = (spawnPoints[n].x-1)*32+player.sprite.w/2
        player.sprite.y = (spawnPoints[n].y-1)*32+player.sprite.h/2+16
      else
        player.toBeRespawn = true
        --player.sprite.physics:setTransform((spawnPoints[n].x-1)*32+player.sprite.w/2,(spawnPoints[n].y-1)*32+player.sprite.h/2+16,0)
      end
      player.sprite.isVisible = true
      physics:resume()
    end
  else
    physics:pause()
    player.sprite.isVisible = false
    system:addTimer(respawn,1,RESPAWN_TIME+1)
  end
end

----WORKFLOW
function scene:apply(content)
  --Load map
  map = Map:new(content)
  sceneRoot:addChild(map.sprite)
  --Create ownPlayer
  player = Player:new({source="player"})
  sceneRoot:addChild(player.sprite)
  --Spawn
  respawn(player,map)
  --Attach wepons
  local weaponController = WeaponController:new(player,map)
  sceneRoot:addChild(weaponController.node)
  --Attach camera
  local camera = Camera:new(sceneRoot,player)
  --Attach physics
  local physicsInterface = PhysicsInterface:new(map,player)
  --Attach controlls
  local control = Control:new(player,weaponController)
  controlRoot:addChild(control.sprite)
  --Attach photonClient
  photonClient:attachPlayer(player)
  photonClient:attachWeaponController(weaponController)
  --Show enemies
  enemies = {}
  --Draw UI
  uiBarContainer = UIBarContainer:new(player,weaponController)
  controlRoot:addChild(uiBarContainer.node)
  --Handlers
  function handler_systemUpdate(event)
    camera:update()
    physicsInterface:update()
    weaponController:update()
    uiBarContainer:update()
    
    --remove nodes
    local nodeToRemove
    while (not(scene_game_nodesToRemove:isEmpty())) do
      nodeToRemove = scene_game_nodesToRemove:pop()
      physics:removeNode(nodeToRemove)
      nodeToRemove = nodeToRemove:removeFromParent()
    end
    collectgarbage()
  end
  system:addEventListener("update",handler_systemUpdate)

  function handler_keyUpdate(event)
    control:keyListener(event)
  end
  system:addEventListener("key",handler_keyUpdate)
  
  
  function handler_photonClientEvent(event)
    --dbg.dbg.print(inspect(event.content))
    if (event.content.type == "update" and not(event.content.player == player.nick)) then
      --dbg.dbg.print("enemyUpdate")
      if (event.content.bulletId) and (enemies[event.content.player]) then
        if not(enemies[event.content.player].bullets[event.content.bulletId]) then
          enemies[event.content.player].bullets[event.content.bulletId] = weaponController_createBullet(event.content.bulletId)
          sceneRoot:addChild(enemies[event.content.player].bullets[event.content.bulletId])
          physics:addNode(enemies[event.content.player].bullets[event.content.bulletId],{type = "kinematic", friction = 0, restitution = 0})
          enemies[event.content.player].bullets[event.content.bulletId]._debug_ = "ENEMY_BULLET"
          enemies[event.content.player].bullets[event.content.bulletId]:addEventListener("collision",weaponController_collisionHandler)
        end
        enemies[event.content.player].bullets[event.content.bulletId].physics:setTransform(event.content.x,event.content.y,event.content.rotation*event.content.multiplier)
      else
        if not(enemies[event.content.player]) then
          enemies[event.content.player] = Player:new({source = "f_player"})
          sceneRoot:addChild(enemies[event.content.player].sprite)
          physics:addNode(enemies[event.content.player].sprite,{type = "kinematic", friction = 0, restitution = 0})
          enemies[event.content.player].bullets = {}
          enemies[event.content.player].sprite._debug_ = "ENEMY_BULLET"
          enemies[event.content.player].sprite:addEventListener("collision",weaponController_collisionHandler)
        end
        enemies[event.content.player].sprite.physics:setTransform(event.content.x,event.content.y,event.content.rotation)
      end
    elseif (event.content.type == "explode") then
      if (enemies[event.content.player]) then
        if (enemies[event.content.player].bullets[event.content.bulletId]) then
          physics:removeNode((enemies[event.content.player].bullets[event.content.bulletId]))
          enemies[event.content.player].bullets[event.content.bulletId] = enemies[event.content.player].bullets[event.content.bulletId]:removeFromParent()
          local dmg = MAX_DAMAGE - MAX_DAMAGE*distanceBetweenPoints(player.sprite,event.content)/DAMAGE_DISTANCE
          if (dmg >= 0) then
            print(dmg)
            player:dealDamage(dmg,event.content.player)
          end
        end
      end
    end
    --[[elseif (event.content.type == "damage") then
      dbg.dbg.print(event.content.reciever.." "..player.nick)
    elseif (event.content.type == "damage" and event.content.reciever == player.nick) then
      player:dealDamage(event.content.deltDamage,event.content.player)
    end]] 
  end
  system:addEventListener("photonClientEvent",handler_photonClientEvent)
  --[[function handler_collision(event)
    player:collisionHandler(event)
  end
  
  player.sprite:addEventListener("collision",handler_collision)
  for y = 1, map.mapObject.mapHeight do 
    if (map.sprite.spriteMatrix[y] == nil) then map.sprite.spriteMatrix[y] = {} end
    for x = 1, map.mapObject.mapWidth do
      if (map.sprite.spriteMatrix[y][x]) then map.sprite.spriteMatrix[y][x]:addEventListener("collision",handler_collision) end
    end
  end]]
  --SCENE FOOTER
end
return scene