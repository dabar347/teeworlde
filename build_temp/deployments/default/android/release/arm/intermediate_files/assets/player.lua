-- Your app starts here!
print("player.lua")

Player = inheritsFrom(baseClass)

function Player:new(config)
  local o = Player:create()
  Player:init(o,config)
  return o
end

function Player:init(o,config)
  o:createCharacter(config)
  o.vy = 0
  o.vx = 0
end


function Player:createCharacter(config)
	local player = director:createSprite({
	    w = CHAR_WIDTH, --CHAR_HEIGHT/2
	    h = CHAR_HEIGHT,
	    zOrder = 2,
      isPlayer = true,
      vx = 0,
      vy = 0,
	    xAnchor = 0.5,
	    yAnchor = 0.5,
	    source=config.source..".png",
	    grounded = 0,
      time = 0
	  })
  print(player.w)
  math.randomseed(os.time())
  self.sprite = player
  self.nick = "user"..math.random(100)
end

function Player:setMotion(motion)
  if motion == "left" then
    self.vx = -X_VELOCITY_CONST
  elseif motion == "right" then
    self.vx = X_VELOCITY_CONST
  elseif motion == "still" then
    self.vx = 0
  elseif motion == "jump" then
    if (true) then self.vy = Y_VELOCITY_CONST end
  end
end

--OLD ONES DOWN THERE--

function leftEventListener(event)
  if event.phase == "began" then
    leftHorizontal(player)
  else 
  	stopHorizontal(player)
  end
end

function rightEventListener(event)
  if event.phase == "began" then
    rightHorizontal(player)
  else 
  	stopHorizontal(player)
  end
end

function checkBoxes(x,y)
  local table = {}
  --print(x)
  --bottom left
  table.bottomLeft = checkBlock(x-CHAR_WIDTH/2,y-CHAR_HEIGHT/2)
  table.bottomRight = checkBlock(x+CHAR_WIDTH/2,y-CHAR_HEIGHT/2)
  table.topLeft = checkBlock(x-CHAR_WIDTH/2,y+CHAR_HEIGHT/2)
  table.topRight = checkBlock(x+CHAR_WIDTH/2,y+CHAR_HEIGHT/2)
  return {
    top = table.topLeft or table.topRight,
    left = table.bottomLeft or table.topLeft,
    right = table.bottomRight or table.topRight,
    bottom = table.bottomLeft or table.bottomRight
  }
end

function updatePlayer(event)
  --local dt = event.time - player.time
  --player.tim  e = event.time
  local vy = player.vy
  if vy == 0 then
    dt, vy = player.physics:getLinearVelocity()
  end
  --player.physics:setTransform(player.x,player.y,0)
  player.physics:setLinearVelocity(player.vx,player.vy)
  player.vy = player.vy - Y_ACCELERATION_CONST
  
  --client-server server
  local data = {
    player = {
      nick = player.nick,
      x = player.x,
      y = player.y
    },
    shoot = nil,
    type = "current"
  }
  --print(type(photonClient))
  photonClient:update(data)
  photonClient:service()
  
  reinitCamera()
end

function playerSensor(event)
  if (event.phase == "began") then
    if (event.nodeB.isPlayer) then
      event.nodeB.grounded = event.nodeB.grounded + 1
    end
  elseif (event.phase == "ended") then
    event.nodeB.grounded = event.nodeB.grounded - 1
  end
end

function setPlayerCoord(player,x,y)
	player.x = x
	player.y = y
end

function leftHorizontal(target)
  target.vx = -X_VELOCITY_CONST
end

function rightHorizontal(target)
  target.vx = X_VELOCITY_CONST
end

function stopHorizontal(target)
  target.vx = 0
end

function jump(target)
  if (player.grounded > 0) then target.vy = Y_VELOCITY_CONST end
end