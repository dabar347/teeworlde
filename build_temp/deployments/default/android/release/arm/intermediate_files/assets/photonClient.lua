-- runs PEER_COUNT instances of peer, each sends init message and afterward replies on every received

require("photon")
local socket = require("socket")

local LoadBalancingClient = require("photon.loadbalancing.LoadBalancingClient")
local LoadBalancingConstants = require("photon.loadbalancing.constants")
local tableutil = require("photon.common.util.tableutil")
local PhotonTime = require("photon.common.util.time")

local MESSAGES_COUNT = -1 -- unlimited if < 0
local MESSAGES_DELAY = 2
local PEER_COUNT = 10
local EVENT_CODE = 11
local hellos = {"Hey!","What?s up?","Wassup?","What?s going down!","How?s it going?","How YOU doing?","G?day!","?Sup?","What?s shakin??","Howdy!","Aloha!","What?s new?","Halloo","Hullo","Hail!","Shalom!","Peace!","Yoo hoo!","Yo!","Hey there!","Word!","Alright!","A-ight!","Top of the morning to ya!","G?day Mate!","Dude!","Hiya!","ello!","Hey ya!","Hey baby!","Yoyoyo!","Whattup?","Whazzzzzzzzzzzzup?","What?s happening?","Cheers!","Howzit?"}

local _P = function(x) print(tableutil.toStringReq(x)) end

local players = {}

function updateMultiplayer(data)
  --print("updatePlayer")
  if (players[data.player.nick] == nil) then
    players[data.player.nick] = createCharacter({source = "f_player"})
    root:addChild(players[data.player.nick])
  end
  players[data.player.nick].nick = data.player.nick
  players[data.player.nick].x = data.player.x
  players[data.player.nick].y = data.player.y
end

local appInfo = require("cloud-app-info")

local function createClient(index)
    local l = LoadBalancingClient.new(appInfo.MasterAddress, appInfo.AppId, appInfo.AppVersion)
    l.logger:setPrefix("C" .. index)
    l:setLogLevel(5)
    l.index = index
    l.name = "user"..index
    print(inspect(l))
    l.msgCnt = 0
    l.msgLastTime = PhotonTime.timeFromStart()
    l.msgToSend = nil
    l.messagesDelay = 0.01--MESSAGES_DELAY + MESSAGES_DELAY * l.index / PEER_COUNT

    function l:onOperationResponse(errorCode, errorMsg, code, content)
        self.logger:info("onOperationResponse", errorCode, errorMsg, code, tableutil.toStringReq(content))
        if errorCode ~= 0 then
            if code == LoadBalancingConstants.OperationCode.JoinRandomGame then  -- master random room fail - try create
                self.logger:info("createRoom")
                self:createRoom("autochat")
            end
            if code == LoadBalancingConstants.OperationCode.CreateGame then -- master create room fail - try join random
                self.logger:info("joinRandomRoom - 2")
                self:joinRandomRoom()
            end
            if code == LoadBalancingConstants.OperationCode.JoinGame then -- game join room fail (probably removed while reconnected from master to game) - reconnect
                self.logger:info("reconnect")
                self:disconnect()
            end
        end
    end

    function l:onStateChange(state)
        self.logger:info("Demo: onStateChange " .. state .. ": " .. tostring(LoadBalancingClient.StateToName(state)))
        if state == LoadBalancingClient.State.JoinedLobby then
            self.logger:info("joinRandomRoom - 1")
            self:joinRandomRoom()
        elseif state == LoadBalancingClient.State.Joined then
            self:sendMessage("hi, " .. " i'm " .. self:myActor().name)
        elseif state == LoadBalancingClient.State.Error then
            self:connect()
        elseif state == LoadBalancingClient.State.Disconnected then
            self:connect()
        end
    end

    function l:onError(errorCode, errorMsg)
    	if errorCode == LoadBalancingClient.PeerErrorCode.MasterAuthenticationFailed then
    		errorMsg = errorMsg .. " with appId = " .. self.appId
    	end
        self.logger:error(errorCode, errorMsg)
    end

    function l:onEvent(code, content, actorNr)
        self.logger:debug("on event", code, tableutil.toStringReq(content))
        if code == EVENT_CODE then
            local mess = content.type
            local actor = content.player.nick
            self.logger:info(actor..": "..mess)
            --print(content.type == "current")
            if (content.type == "current") then
              updateMultiplayer(content)
            end
        end
    end

    function l:sendMessage(data)
        if self.msgLastTime + self.messagesDelay < PhotonTime.timeFromStart() then
            --local data = {message = str, actor = self.name}
            self:raiseEvent(EVENT_CODE, data)
            self.msgLastTime = PhotonTime.timeFromStart()
            self.msgToSend = nil
        else
            self.msgToSend = data
        end
    end

    function l:update(data)
        --[[if self.msgToSend then
            --self.logger:info("From update func")
            self:sendMessage(self.msgToSend)
        end]]
        self:sendMessage(data)
        self:service()
    end

    return l
end

--[[local peers = {}
for i = 1,PEER_COUNT do
    peers[i] = createClient(i)
end
for i, p in pairs(peers) do
    p:connect()
end]]
local client = createClient(2)
client:connect()
client.msgToSend = "qwer"


return client
--[[while true do
    for i, p in pairs(peers) do
        p:update()
        p:service()
    end
    client:update()
    client:service()
    socket.sleep(0.1)
end]]