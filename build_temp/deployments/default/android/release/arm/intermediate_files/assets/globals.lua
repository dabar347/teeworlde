print("globals.lua")

PROCENTAGE_SCALE = 100
MAIN_SCALE = PROCENTAGE_SCALE*director.displayHeight/516/100

EDITOR_MAP_NAME = "mapa"
EDITOR_MAP_WIDTH = 32 --1024px
EDITOR_MAP_HEIGHT = 25 --800px
EDITOR_PLAYER_NUMBER = 4
ATLAS_LENGTH = 2

inspect = require("inspect")

function powerOfTen(power)
  local result = 1
  for i = 1,power do
    result = result * 10
  end
  return result
end

PHYSICS_CONST = powerOfTen(2)
X_VELOCITY_CONST = 3.5*PHYSICS_CONST
Y_VELOCITY_CONST = 5*PHYSICS_CONST
Y_ACCELERATION_CONST = 10*PHYSICS_CONST

CHAR_WIDTH = 28
CHAR_HEIGHT = 48