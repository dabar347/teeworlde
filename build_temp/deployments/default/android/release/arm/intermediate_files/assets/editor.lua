EDITOR_MODE = false


function createPointAt(x, y)
  circle = director:createCircle({
    x = 32*x,
    y = 32*y,
    radius = 3,
    alpha = 1,
    color = color.green,
    strokeWidth = 1,
    isVisible = true,
    zOrder = 10,
   })
  root:addChild(circle)
end

function drawPointGrid()
  for x=0,MAP_WIDTH do
    for y=0,MAP_HEIGHT do
      createPointAt(x,y)
    end
  end
end

function keyListener(event)
  --print(event.keyCode)
  if (event.keyCode == key.Z) then
    if (event.phase == "pressed") then
      mode = 1
      pointerLable.text = "TILE "..pickedTile
    end
  elseif (event.keyCode == key.X) then
    if (event.phase == "pressed") then
      mode = 2
      pointerLable.text = "CHARACTER"
    end
  elseif (event.keyCode == key.C) then
    if (event.phase == "pressed") then
      mode = 3
      pointerLable.text = "DELETE"
    end
  elseif (event.keyCode == key.V) then
    if (event.phase == "pressed") then
      mode = 4
      pointerLable.text = "SAVED"
      saveMap(map,MAP_NAME)
    end
  elseif (event.keyCode >= 14 and event.keyCode <= 22) then
    if (event.phase == "pressed") then
      pickedTile = event.keyCode - 13
      mode = 1
      pointerLable.text = "TILE "..pickedTile
    end
  elseif (event.keyCode == key.W) then
    if (event.phase == "pressed") then
      root.vy = 5
    else
      root.vy = 0
    end
  elseif (event.keyCode == key.S) then
    if (event.phase == "pressed") then
      root.vy = -5
    else
      root.vy = 0
    end
  elseif (event.keyCode == key.A) then
    if (event.phase == "pressed") then
      root.vx = -5
    else
      root.vx = 0
    end
  elseif (event.keyCode == key.D) then
    if (event.phase == "pressed") then
      root.vx = 5
    else
      root.vx = 0
    end
  end
end
system:addEventListener("key",keyListener)

function pointerClick(event)
  if (event.phase == "began") then
    local x = math.floor((event.x-root.x)/32)+1
    local y = math.floor((event.y-root.y)/32)+1
    --print("TRACK")
    if (mode == 1) then
      mapUpdate(x,y,pickedTile+1)
    elseif (mode == 2) then
      mapUpdate(x,y,1)
    elseif (mode == 3) then
      mapUpdate(x,y,0)
    end
  end
end

function editorUpdate(event)
  root.x = root.x - root.vx
  root.y = root.y - root.vy
end

if (EDITOR_MODE) then
  
  dofile("player.lua")
  dofile("map.lua")
  pickedTile = 1
  mode = 1
  root = director:createNode({
      vx = 0,
      vy = 0,
    })
  map = loadMap(MAP_NAME)
  root:addChild(map)
  --director.addNodesToScene = false
  drawPointGrid()
  system:addEventListener("key",keyListener)
  pointerLable = director:createLabel({
      x=0, 
      y=director.displayHeight-20, 
      text="TILE 1",
      color = color.white,
      zOrder=11
    })
  system:addEventListener("touch",pointerClick)
  system:addEventListener("update",editorUpdate)
  
end

return EDITOR_MODE