print ("map.lua")

local mapAtlas = director:createAtlas({
    width = 32,
    height = 32,
    numFrames = ATLAS_LENGTH,
    textureName = "maps/atlas.png",
})

local mapAtlasAnimation = director:createAnimation({
    start = 1,
    count = ATLAS_LENGTH,
    atlas = mapAtlas,
    delay = 1,
})

Map = inheritsFrom(baseClass)

function Map:new(name)
  local o = Map:create()
  Map:init(o,name)
  return o
end

function Map:init(o,name)
  o:loadMap(name)
end

function Map:saveMap(name)
  local path = "maps/" .. name .. ".twm"
  writeMapFile(self.mapObject,path)
end

function Map:loadMap(name)
	-- load maps
	local path = "maps/" .. name .. ".twm"
	self.sprite = director:createNode()
  self.mapObject = readMapFile(path)
  self:drawMap()
	--return map
end

function readMapFile(path)
  --local f = assert(io.open(path,"rb"))
  local f = io.open(path,"rb")
  local mapObject = {}
  if (f == nil) then
    mapObject.mapWidth = MAP_WIDTH
    mapObject.mapHeight = MAP_HEIGHT
    mapObject.playerNumber = PLAYER_NUMBER
    mapObject.matrix = {}
    for y=1,mapObject.mapHeight do
      mapObject.matrix[y] = {}
      for x=1,mapObject.mapWidth do
      --1,1 = 4 (y-1)*mapObject.mapWidth+(x-1)+4
      --2,3 = 9 1*3+2+4=9
        mapObject.matrix[y][x] = 0
      end
    end
    return mapObject
  end
  local string = f:read("*a")
  local byteArray = {string:byte(1,#string)}
  assert(f:close())
  
  mapObject.mapWidth = byteArray[1]
  mapObject.mapHeight = byteArray[2]
  mapObject.playerNumber = byteArray[3]
  mapObject.matrix = {}
  for y=1,mapObject.mapHeight do
    mapObject.matrix[y] = {}
    for x=1,mapObject.mapWidth do
      --1,1 = 4 (y-1)*mapObject.mapWidth+(x-1)+4
      --2,3 = 9 1*3+2+4=9
      mapObject.matrix[y][x] = byteArray[(y-1)*mapObject.mapWidth+(x-1)+4]
    end
  end
  return mapObject
end

function writeMapFile(mapObject,path)
  local byteArray = {}
  byteArray[1] = MAP_WIDTH
  byteArray[2] = MAP_HEIGHT
  byteArray[3] = PLAYER_NUMBER
  print(byteArray[1])
  print(MAP_WIDTH)
  print(byteArray[2])
  print(MAP_HEIGHT)
  print(byteArray[3])
  print(PLAYER_NUMBER)
  for y = 1,MAP_HEIGHT do
    if (mapObject.matrix[y] == nil) then mapObject.matrix[y] = {} end
    for x = 1,MAP_WIDTH do
      if mapObject.matrix[y][x] == nil then
        byteArray[(y-1)*MAP_WIDTH+(x-1)+4] = 0
      else
        byteArray[(y-1)*MAP_WIDTH+(x-1)+4] = mapObject.matrix[y][x]
      end
    end
  end
  local f = assert(io.open(path,"wb"))
  local str = ""
  for i = 1,#byteArray do
    str = str..string.char(byteArray[i])
  end
  assert(f:write(str))
  assert(f:close())
end

function Map:drawMap()
  self.sprite.spriteMatrix = {}
  for _y=1,self.mapObject.mapHeight do
    self.sprite.spriteMatrix[_y] = {}
    for _x=1,self.mapObject.mapWidth do
      --[[local sprite = director:createSprite({
          x = (_x-1)*32,
          y = (_y-1)*32,
          width = 32,
          height = 32,
          source = mapAtlasAnimation,
      })]]
      if (self.mapObject.matrix[_y][_x] >= 2) then 
        local sprite = director:createSprite({
          x = (_x-1)*32,
          y = (_y-1)*32,
          width = 32,
          height = 32,
          source = mapAtlasAnimation,
          solid = true,
        })
        sprite:pause()
        sprite:setFrame(self.mapObject.matrix[_y][_x]-1)
        if (not(EDITOR_MODE)) then physics:addNode(sprite,{type = 'static', isSensor = false, friction = 0, restitution = 0}) end
        self.sprite:addChild(sprite)
        --sprite:addEventListener("collision",playerSensor) --INIT SENSOR
        self.sprite.spriteMatrix[_y][_x] = sprite
      end
      --print(self.sprite.spriteMatrix[_y][_x])
    end
  end
end

function Map:mapUpdate(x,y,new)
  --[[print(map)
  print(map.mapObject)
  print(map.mapObject.matrix)
  print(map.mapObject.matrix[y])
  print(map.mapObject.matrix[y][x])
  print(new)]]
  if (self.mapObject.matrix[y] == nil) then self.mapObject.matrix[y] = {} end
  if (self.sprite.spriteMatrix[y] == nil) then self.sprite.spriteMatrix[y] = {} end
  self.mapObject.matrix[y][x] = new
  if (new > 1) then
    if (self.sprite.spriteMatrix[y][x] == nil) then
      local sprite = director:createSprite({
          x = (x-1)*32,
          y = (y-1)*32,
          width = 32,
          height = 32,
          source = mapAtlasAnimation,
          solid = true,
        })
      sprite:pause()
      self:addChild(sprite)
      sprite:addEventListener("collision",playerSensor)
      self.sprite.spriteMatrix[y][x] = sprite
    end
    self.sprite.spriteMatrix[y][x]:setFrame(self.mapObject.matrix[y][x]-1)
  else
    if (not(self.sprite.spriteMatrix[y][x] == nil)) then
      self.sprite.spriteMatrix[y][x] = self.sprite.spriteMatrix[y][x]:removeFromParent()
      collectgarbage()
    end
  end
end

function Map:getSpawnPoints()
  local spawnPoints = {}
  for _y = 1,self.mapObject.mapHeight do
    --if (self.mapObject.matrix[_y] == nil) then self.mapObject.matrix[_y] = {} end
    for _x=1,self.mapObject.mapWidth do
      --print(inspect(self.mapObject.matrix[_y][_x]))
      if (self.mapObject.matrix[_y][_x] == 1) then spawnPoints[#spawnPoints] = {x = _x, y = _y} end
    end
  end
  return spawnPoints
end

--[[function checkBlock(_x,_y)
  local x = math.floor(_x/32)+1
  local y = math.floor(_y/32)+1
  if (self.sprite.spriteMatrix[y] == nil) then return false end
  if (self.sprite.spriteMatrix[y][x] == nil) then return false end
  return self.sprite.spriteMatrix[y][x].solid
end]]