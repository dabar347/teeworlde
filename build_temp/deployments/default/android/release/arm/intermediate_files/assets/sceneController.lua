local SceneController = inheritsFrom(baseClass)

function SceneController:new()
  local o = SceneController:create()
  SceneController:init(o)
  return o
end

function SceneController:init(o)
  o.list = {}
end

function SceneController:changeScene(name,content)
  if (self.list[name] == nil) then
    self:loadScene(name)
  end
  director:moveToScene(self.list[name])
  print(content)
  if (content) then
    self.list[name]:apply(content)
  end
end

function SceneController:loadScene(name)
  self.list[name] = require("scene_"..name)
end

sceneController = SceneController:new()
sceneController:changeScene("MainMenu")