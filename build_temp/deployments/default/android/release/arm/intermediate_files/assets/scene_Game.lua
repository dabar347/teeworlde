local scene = director:createScene()
director.addNodesToScene = true
local sceneRoot = director:createNode({xScale = MAIN_SCALE, yScale = MAIN_SCALE})
director.addNodesToScene = false
--SCENE HEADER
--Requires
require("globals")
require("map")
require("player")
require("camera")
require("physicsInterface")
require("control")
----WORKFLOW
function scene:apply(content)
  --Connect to game
  --!sceneController.room!
  photonClient = {room = { map = "mapa" }} --example without connection. Because it sucks
  --Load map
  local map = Map:new(photonClient.room.map)
  sceneRoot:addChild(map.sprite)
  --Create ownPlayer
  local player = Player:new({source="player"})
  sceneRoot:addChild(player.sprite)
  --Spawn
  function respawn(player,map)
    local spawnPoints = map:getSpawnPoints()
    math.randomseed(os.time())
    local n = math.random(#spawnPoints+1) - 1
    if (player.sprite.physics == nil) then
      player.sprite.x = (spawnPoints[n].x-1)*32+player.sprite.w/2
      player.sprite.y = (spawnPoints[n].y-1)*32+player.sprite.h/2+16
    else
      player.sprite.physicssetTransform((spawnPoints[n].x-1)*32+player.sprite.w/2,(spawnPoints[n].y-1)*32+player.sprite.h/2+16,0)
    end
  end

  respawn(player,map)
  --Attach camera
  local camera = Camera:new(sceneRoot,player)
  --Attach physics
  local physicsInterface = PhysicsInterface:new(map,player)
  --Attach controlls
  local control = Control:new(player)
  --Show enemies

  --Handlers
  function handler_systemUpdate(event)
    camera:update()
    physicsInterface:update()
  end
  system:addEventListener("update",handler_systemUpdate)

  function handler_keyUpdate(event)
    control:keyListener(event)
  end
  system:addEventListener("key",handler_keyUpdate)
  --SCENE FOOTER
end
return scene