local scene = director:createScene()
director.addNodesToScene = true
local sceneRoot = director:createNode({xScale = MAIN_SCALE, yScale = MAIN_SCALE})
director.addNodesToScene = false
--SCENE HEADER
--Requires
require("globals")
----WORKFLOW
--Establish connection
photonClient = require("photonClient")
--Handlers

--SCENE FOOTER
return scene