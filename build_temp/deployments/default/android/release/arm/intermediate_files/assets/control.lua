print("control.lua")

Control = inheritsFrom(baseClass)

function Control:new(player)
  local o = Control:create()
  Control:init(o,player)
  return o
end

function Control:init(o,player)
  o.player = player
end

function Control:keyListener(event)
  --print(event.keyCode)
  if (event.keyCode == key.W) then
    if (event.phase == "pressed") then
      self.player:setMotion("jump")
    end
  elseif (event.keyCode == key.A) then
    if (event.phase == "pressed") then
      self.player:setMotion("left")
    else
      self.player:setMotion("still")
    end
  elseif (event.keyCode == key.D) then
    if (event.phase == "pressed") then
      self.player:setMotion("right")
    else
      self.player:setMotion("still")
    end
  end
end